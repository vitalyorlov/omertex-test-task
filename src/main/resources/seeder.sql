USE `mydb` ;

INSERT INTO  `mydb`.`topic` (`id`, `name`) VALUES (NULL,  'First topic');
INSERT INTO  `mydb`.`topic` (`id`, `name`) VALUES (NULL,  'Second topic');
INSERT INTO  `mydb`.`topic` (`id`, `name`) VALUES (NULL,  'Third topic');

INSERT INTO `inquiry` (`id`, `description`, `create_date`, `customer_name`, `topic_id`) VALUES (NULL, 'some description', '2016-08-09', 'Andrey', '1');
INSERT INTO `inquiry` (`id`, `description`, `create_date`, `customer_name`, `topic_id`) VALUES (NULL, 'some description', '2016-08-10', 'Andrey', '2');
INSERT INTO `inquiry` (`id`, `description`, `create_date`, `customer_name`, `topic_id`) VALUES (NULL, 'some description', '2016-08-11', 'Alexey', '3');
INSERT INTO `inquiry` (`id`, `description`, `create_date`, `customer_name`, `topic_id`) VALUES (NULL, 'some description', '2016-08-09', 'Alexey', '1');
INSERT INTO `inquiry` (`id`, `description`, `create_date`, `customer_name`, `topic_id`) VALUES (NULL, 'some description', '2016-08-09', 'Alexey', '1');
INSERT INTO `inquiry` (`id`, `description`, `create_date`, `customer_name`, `topic_id`) VALUES (NULL, 'some description', '2016-08-09', 'Oleg', '2');