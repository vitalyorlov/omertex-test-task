package com.vitalyorlov.testtask.dao;

import com.vitalyorlov.testtask.model.Topic;
import com.vitalyorlov.testtask.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.List;

public class TopicDAO {
    public static List<Topic> getTopics() {
        Session session = HibernateUtil.makeSession();
        session.beginTransaction();
        List<Topic> topicsList = null;
        try {
            topicsList = (List<Topic>)session.createCriteria(Topic.class).list();
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

        return topicsList;
    }
}
