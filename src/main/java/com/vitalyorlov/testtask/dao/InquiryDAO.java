package com.vitalyorlov.testtask.dao;

import com.vitalyorlov.testtask.model.Inquiry;
import com.vitalyorlov.testtask.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class InquiryDAO {

    public static void addOrUpdateInquiry(Inquiry inquiry) {
        Session session = HibernateUtil.makeSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(inquiry);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public static void deleteInquiryByIdAndUsername(long id, String username) {

        Session session = HibernateUtil.makeSession();
        try {
            session.beginTransaction();
            Inquiry inquiry = session.get(Inquiry.class, id);

            if (inquiry != null && inquiry.getCustomerName().equals(username)) {
                session.delete(inquiry);
            }
            session.getTransaction().commit();
        } catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

    }

    public static Inquiry getInquiryByIdAndUsername(long id, String username) {
        Session session = HibernateUtil.makeSession();
        session.beginTransaction();
        Inquiry inquiry = null;
        try {
            Criteria criteria = session.createCriteria(Inquiry.class);
            criteria.add(Restrictions.eq("id", id));
            inquiry = (Inquiry)criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

        if (inquiry.getCustomerName().equals(username))
            return inquiry;
        else
            return null;
    }

    public static List<Inquiry> getInquiriesByUsername(String username) {
        Session session = HibernateUtil.makeSession();
        session.beginTransaction();
        List<Inquiry> inquiriesList = null;
        try {
            Criteria criteria = session.createCriteria(Inquiry.class);
            criteria.add(Restrictions.eq("customerName", username));
            inquiriesList = (List<Inquiry>)criteria.list();
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

        return inquiriesList;
    }
}
