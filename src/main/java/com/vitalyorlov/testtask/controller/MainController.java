package com.vitalyorlov.testtask.controller;

import com.vitalyorlov.testtask.dao.InquiryDAO;
import com.vitalyorlov.testtask.dao.TopicDAO;
import com.vitalyorlov.testtask.model.Inquiry;
import com.vitalyorlov.testtask.model.Topic;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;

@RestController
public class MainController {

    @RequestMapping("/")
    public String welcome() {
        return "Omertex test task";
    }

    @RequestMapping(value = "/topics", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Topic>> getTopics() {
        List<Topic> topics = TopicDAO.getTopics();

        if(topics.isEmpty()){
            new ResponseEntity<List<Topic>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Topic>>(topics, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/{customerName}/inquiries",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Inquiry>> getInquiriesByCustomerName(@PathVariable String customerName) {
        List<Inquiry> inquiries = InquiryDAO.getInquiriesByUsername(customerName);

        if(inquiries.isEmpty()){
            new ResponseEntity<List<Inquiry>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Inquiry>>(inquiries, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/{customerName}/inquiries/{inquiryId}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Inquiry> getInquiryByIdAndCustomerName(@PathVariable String customerName,
                                                                 @PathVariable long inquiryId) {
        Inquiry inquiry = InquiryDAO.getInquiryByIdAndUsername(inquiryId, customerName);

        if(inquiry == null){
            new ResponseEntity<Inquiry>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Inquiry>(inquiry, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/{customerName}/inquiries", method = RequestMethod.POST)
    public ResponseEntity<Void> createInquiry(@PathVariable String customerName,
                                              @RequestBody Inquiry inquiry) {
        inquiry.setCustomerName(customerName);
        inquiry.setCreateDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        InquiryDAO.addOrUpdateInquiry(inquiry);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/customers/{customerName}/inquiries/{inquiryId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateInquiry(@PathVariable String customerName,
                                              @PathVariable long inquiryId,
                                              @RequestBody Inquiry inquiry) {
        if (InquiryDAO.getInquiryByIdAndUsername(inquiryId, customerName) == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        InquiryDAO.addOrUpdateInquiry(inquiry);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/customers/{customerName}/inquiries/{inquiryId}", method = RequestMethod.DELETE)
    public ResponseEntity<Inquiry> deleteInquiry(@PathVariable String customerName, @PathVariable long inquiryId) {
        InquiryDAO.deleteInquiryByIdAndUsername(inquiryId, customerName);

        return new ResponseEntity<Inquiry>(HttpStatus.NO_CONTENT);
    }

}
