package dao;

import com.vitalyorlov.testtask.dao.InquiryDAO;
import com.vitalyorlov.testtask.model.Inquiry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.util.Calendar;

import static org.junit.Assert.assertNotNull;

public class InquiryDaoTest {
    private Inquiry inquiry;

    @Before
    public void setUp() throws Exception {
        inquiry = new Inquiry();
        inquiry.setDescription("description");
        inquiry.setCustomerName("Alexey");
        inquiry.setTopicId(1);
        inquiry.setCreateDate(new Date(Calendar.getInstance().getTime().getTime()));
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void addInquiry() throws Exception {
        InquiryDAO.addOrUpdateInquiry(inquiry);
    }

    @Test
    public void updateInquiry() throws Exception {
        inquiry.setId(2);
        InquiryDAO.addOrUpdateInquiry(inquiry);
    }

    @Test
    public void deleteInquiry() throws Exception {
        InquiryDAO.deleteInquiryByIdAndUsername(2, "Alexey");
    }

    @Test
    public void getInquiriesByUsername() throws Exception {
        assertNotNull(InquiryDAO.getInquiriesByUsername("Andrey"));
    }

    @Test
    public void getInquiryByIdAndUsername() throws Exception {
        assertNotNull(InquiryDAO.getInquiryByIdAndUsername(1, "Andrey"));
    }
}
