package controller;


import com.vitalyorlov.testtask.controller.MainController;
import com.vitalyorlov.testtask.dao.InquiryDAO;
import com.vitalyorlov.testtask.dao.TopicDAO;
import com.vitalyorlov.testtask.model.Inquiry;
import com.vitalyorlov.testtask.model.Topic;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;

import java.net.URI;
import java.sql.Date;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MainControllerTest {
    private MainController controller;
    private String username = "Alexey";
    public static final String REST_SERVICE_URI = "http://localhost:8080";

    @Before
    public void setUp() throws Exception {
        controller = new MainController();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getTopics() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> topicsMap =
                restTemplate.getForObject(REST_SERVICE_URI + "/topics", List.class);

        if (topicsMap != null) {
            for(LinkedHashMap<String, Object> map : topicsMap) {
                System.out.println("Topic : id=" + map.get("id")
                        + ", Name=" + map.get("name"));
            }
        } else {
            System.out.println("No topics exist----------");
        }
    }

    @Test
    public void getInquiriesByCustomerName() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> inquiriesMap =
                restTemplate.getForObject(REST_SERVICE_URI + "/customers/" + username + "/inquiries", List.class);

        if (inquiriesMap != null) {
            for(LinkedHashMap<String, Object> map : inquiriesMap) {
                System.out.println("Inquiry : id=" + map.get("id")
                        + ", Description=" + map.get("description")
                        + ", CustomerName=" + map.get("customerName")
                        + ", TopicId=" + map.get("topicId")
                        + ", CreateDate=" + map.get("createDate"));
            }
        } else {
            System.out.println("No inquiries exist----------");
        }
    }

    @Test
    public void getInquiryByIdAndCustomerName() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        Inquiry inquiry = restTemplate.getForObject(REST_SERVICE_URI + "/customers/"
                        + username + "/inquiries/6", Inquiry.class);

        if (inquiry != null) {
            System.out.println("Inquiry : id=" + inquiry.getId()
                        + ", Description=" + inquiry.getDescription()
                        + ", CustomerName=" + inquiry.getCustomerName()
                        + ", TopicId=" + inquiry.getTopicId()
                        + ", CreateDate=" + inquiry.getCreateDate());
        } else {
            System.out.println("No inquiries exist----------");
        }
    }

    @Test
    public void createInquiry() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        Inquiry inquiry = new Inquiry();
        inquiry.setDescription("description");
        inquiry.setCustomerName("Alexey");
        inquiry.setTopicId(1);
        inquiry.setCreateDate(new Date(Calendar.getInstance().getTime().getTime()));
        restTemplate.postForLocation(REST_SERVICE_URI + "/customers/" + username + "/inquiries",
                inquiry, Inquiry.class);
        System.out.println(inquiry);
    }

    @Test
    public void updateInquiry() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        Inquiry inquiry = new Inquiry();
        inquiry.setDescription("description");
        inquiry.setCustomerName("Alexey");
        inquiry.setTopicId(1);
        inquiry.setCreateDate(new Date(Calendar.getInstance().getTime().getTime()));
        restTemplate.put(REST_SERVICE_URI + "/customers/" + username + "/inquiries/5",
                inquiry, Inquiry.class);
        System.out.println(inquiry);
    }

    @Test
    public void delete() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI + "/customers/" + username + "/inquiries/4");
    }
}
