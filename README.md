# Test task for omertex #

### Create database ###

cd src/main/resources
mysql -u root -p < schema.sql

### For filling database ###

cd src/main/resources
mysql -u root -p < seeder.sql

## Before running, modify the file hibernate.cfg.xml ##